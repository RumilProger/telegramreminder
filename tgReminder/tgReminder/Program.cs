using System;
using Telegram.Bot;
using Telegram.Bot.Extensions;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using System.Threading;
using System.Threading.Tasks;
using System.Globalization;
using MySqlConnector;
using System.Data;
using System.Collections.Generic;
using Microsoft.Data.Sqlite;

namespace tgReminder
{
    class Program
    {
        static DB db = new DB();
        static List<Tuple<int,string,string,string>> items = new List<Tuple<int, string,string,string>>();

        public class Printer
        {
            public void printNumber(object data)
            {
                TelegramBotClient botcl = (TelegramBotClient)data;

                var command = db.getConnection().CreateCommand();
                command.CommandText =  ("SELECT * FROM Users");

                db.openConnection();
                using (SqliteDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var fir = reader.GetValue(1);
                        var tr = reader.GetValue(2);
                        var fo = reader.GetValue(3);
                        var mes = reader.GetValue(4);

                        Console.WriteLine(fir);
                        Console.WriteLine(tr);
                        Console.WriteLine(fo);
                        items.Add(new Tuple<int, string, string, string>((int)(long)fir, (string)tr, (string)fo, (string)mes));
                    }
                }
                
                db.closeConnection();
                while (true)
                    try
                    {
                        Thread.Sleep(1000);
                        foreach (Tuple<int, string,string,string> s in items)
                        {
                            if (s.Item2 + ":00" == DateTime.Now.ToString().Substring(11) && s.Item3.Contains(DateTime.Now.DayOfWeek.ToString()))
                            {
                                Console.WriteLine("Res");
                                botcl.SendTextMessageAsync(s.Item1, s.Item4);
                            }
                        }
                    }
                    catch
                    {

                    }
            }
        }

        static void Main(string[] args)
        {
            
            
            checkOnDate("23:24 - 1,2,3,4,5,6");

            
            Console.WriteLine(DateTime.Now.ToString().Substring(11, 5));
            
            var botClient = new TelegramBotClient("2125884152:AAFXfwQtBhfK0i8FayW5fnnlYE_e5hym8Tc");
            Printer p = new Printer();
            Thread timeChecker = new Thread(new ParameterizedThreadStart(p.printNumber));
            timeChecker.Start(botClient);
            Thread.Sleep(10000);
            
            
            using var cts = new CancellationTokenSource();
            var receiverOptions = new ReceiverOptions
            {
                AllowedUpdates = { }
            };
            botClient.StartReceiving(
                HandleUpdateAsync,
                HandleErrorAsync,
                receiverOptions,
                cancellationToken: cts.Token
                );

            var me = botClient.GetMeAsync();

            Console.WriteLine($"Start listening for @{me.Result.Username}");
            Console.ReadLine();
            
        }

        static async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            if (update.Type != UpdateType.Message)
                return;

            if (update.Message!.Type != MessageType.Text)
                return;

            var chatId = update.Message.Chat.Id;
            var messageText = update.Message.Text;

            Console.WriteLine($"Received a '{messageText}' message in chat {chatId}.");

           

            if (checkOnDate(messageText))
            {

                var command = db.getConnection().CreateCommand();
                command.CommandText = $"INSERT INTO Users (chatid, data, den, word) VALUES ('{chatId}', '{getData(messageText)}', '{getDayOfWeek(getDen(messageText))}', '{getMesage(messageText)}')";
                db.openConnection();
                command.ExecuteNonQuery();

                items.Add(new Tuple<int, string,string,string>((int)chatId, getData(messageText), getDayOfWeek(getDen(messageText)),getMesage(messageText)));
                Message sentMessage = await botClient.SendTextMessageAsync(
                    chatId: chatId,
                    text: "Вы добавили напоминание:\n" + messageText + "\n" + getDenForMessage(getDen(messageText)),
                    cancellationToken: cancellationToken);
                

                
                db.closeConnection();
            }

            if (messageText == "/setting")
            {
                Message sentMessage = await botClient.SendTextMessageAsync(
                    chatId: chatId,
                    text: "1)понедельник: \n2)вторник: \n3)среда: \n4)пятница: \n5)суббота: \n6)воскресенье: \n" +
                    "на какой день хотите поставить напоминалку?",
                    cancellationToken: cancellationToken);
            }

            if (messageText == "/help" || messageText == "/start")
            {
                Message sentMessage = await botClient.SendTextMessageAsync(
                    chatId: chatId,
                    text: "Чтобы поставить напоминалку введите '00:00-1,7(Покушать яблоко))'\n1 и 7 это дни недели",
                    cancellationToken: cancellationToken);
            }

            
            if (messageText == "/delete")
            {
                items.Clear();
                var command = db.getConnection().CreateCommand();
                command.CommandText = $"DELETE FROM Users WHERE chatid = '{chatId}'";
                db.openConnection();
                command.ExecuteNonQuery();
                db.closeConnection();

                Message sentMessage = await botClient.SendTextMessageAsync(
                    chatId: chatId,
                    text: "Ваши напоминания удалились",
                    cancellationToken: cancellationToken);
            }
        }

        static public bool checkOnDate(string message)
        {
            if (message.Length < 6)
            {
                return false;
            }
            message = message.Replace(" ", "");
            if (message[5] != '-')
            {
                return false;
            }
            
            string time = message.Substring(0, 5);
            Console.WriteLine(time);
            string pattern = "H:mm";
            DateTime parsedDate;
            if (DateTime.TryParseExact(time, pattern, null,
                                    DateTimeStyles.None, out parsedDate))
            {
                Console.WriteLine("Converted '{0}' to {1:H:mm}.",
                                    message, parsedDate);
                return true;
            }
            else
            {
                Console.WriteLine("Unable to convert '{0}' to a date and time.",
                                    message);
                return false;
            }
            
        }

        static public string getData(string message)
        {
            message = message.Replace(" ", "");
            string time = message.Substring(0, 5);
            return time;
            //string pattern = "HH:mm";
            //return DateTime.ParseExact(time, pattern, System.Globalization.CultureInfo.InvariantCulture);
        }

        static public string getDen(string message)
        {
            message = message.Replace(" ", "");

            return message.Substring(6,message.IndexOf('(')-6);

        }

        static public string getMesage(string message)
        {
            message = message.Substring(message.IndexOf('('));
            message = message.Replace("(", "");
            message = message.Replace(")", "");
            return message;
        }

        static public string getDayOfWeek(string message)
        {
            string dni = "";
            if (message.Contains('1'))
            {
                dni = dni + ", Monday";
            }
            if (message.Contains('2'))
            {
                dni = dni + ", Tuesday";
            }
            if (message.Contains('3'))
            {
                dni = dni + ", Wednesday";
            }
            if (message.Contains('4'))
            {
                dni = dni + ", Thursday";
            }
            if (message.Contains('5'))
            {
                dni = dni + ", Friday";
            }
            if (message.Contains('6'))
            {
                dni = dni + ", Saturday";
            }
            if (message.Contains('7'))
            {
                dni = dni + ", Sunday";
            }
            return dni.Substring(1);

        }

        static public string getDenForMessage(string message)
        {
            string dni = "";
            if (message.Contains('1'))
            {
                dni = dni + ", понедельник";
            }
            if (message.Contains('2'))
            {
                dni = dni + ", вторник";
            }
            if (message.Contains('3'))
            {
                dni = dni + ", среда";
            }
            if (message.Contains('4'))
            {
                dni = dni + ", четверг";
            }
            if (message.Contains('5'))
            {
                dni = dni + ", пятница";
            }
            if (message.Contains('6'))
            {
                dni = dni + ", суббота";
            }
            if (message.Contains('7'))
            {
                dni = dni + ", воскрсенье";
            }
            return dni.Substring(1);
        }
        /*
        static public bool regged(long cId)
        {
            DataTable table = new DataTable();
            MySqlDataAdapter adapter = new MySqlDataAdapter();

            MySqlCommand command1 = new MySqlCommand("SELECT * FROM `users2` WHERE `id` = @id",db.getConnection());
            command1.Parameters.Add("@id", MySqlDbType.VarChar).Value = cId;

            adapter.SelectCommand = command1;
            adapter.Fill(table);

            if (table.Rows.Count < 1)
                return false;

            return true;
        }
        */
        static Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            var ErrorMessage = exception switch
            {
                Telegram.Bot.Exceptions.ApiRequestException apiRequestException
                    => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
                _ => exception.ToString()
            };

            Console.WriteLine(ErrorMessage);
            return Task.CompletedTask;
        }
    }
}
