FROM mcr.microsoft.com/dotnet/runtime:5.0
COPY /tgReminder/tgReminder/bin/Release/net5.0/publish/ App/
WORKDIR /App
ENTRYPOINT ["dotnet", "tgReminder.dll"]